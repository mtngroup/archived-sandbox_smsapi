const express = require('express');
const app = express();
app.use(express.json());

console.log('node.js application starting...');

const Status = {
    SUCCESSFUL: "Successful",
    UNSUCCESSFUL: "Unsuccessful"
};
const DeliveryStatus = {
    DELIVERED_TO_NETWORK:"DELIVERED_TO_NETWORK",
    DELIVERY_UNCERTAIN:"DELIVERY_UNCERTAIN",
    DELIVERY_IMPOSSIBLE:"DELIVERY_IMPOSSIBLE",
    MESSAGE_WAITING:"MESSAGE_WAITING",
    DELIVERED_TO_TERMINAL:"DELIVERED_TO_TERMINAL",
    DELIVERY_NOTIFICATION_NOT_SUPPORTED:"DELIVERY_NOTIFICATION_NOT_SUPPORTED",
    PENDING:"PENDING",
    UNSUCCESSFUL:"UNSUCCESSFUL"
};

class APIResponse {
    constructor(links) {
        this._links = links;
    }
}

class Link {
    constructor(deprecation, href, hreflang, media, rel, templated, title, type) {
        this.deprecation = deprecation;
        this.href = href;
        this.hreflang = hreflang;
        this.media = media;
        this.rel = rel;
        this.templated = templated;
        this.title = title;
        this.type = type;
    }
}

const linkEmpty = new Link(null, null, null, null, null, false, null, null);

class SendSms {
    constructor(messageId, smsDeliveryStatus, smsStatusURL, statusCode, to, traceId) {
        this.messageId = messageId;
        this.smsDeliveryStatus = smsDeliveryStatus;
        this.smsStatusURL = smsStatusURL;
        this.statusCode = statusCode;
        this.to = to;
        this.traceId = traceId;
    }
}
const sendSmsEmpty = new SendSms(null, DeliveryStatus.PENDING, null, null, [null, null], null);

class SendSmsRequest {
    constructor(body, from, notificationUrl, systemId, to) {
        this.body = body;
        this.from = from;
        this.notificationUrl = notificationUrl;
        this.systemId = systemId;
        this.to = to;
    }
}
const sendSmsRequestEmpty = new SendSmsRequest(null, null, null, null, [null, null]);

class SmsDeliveryInformation {
    constructor(deliveryStatus, receivedTimestamp, sentTimestamp, to) {
        this.deliveryStatus = deliveryStatus;
        this.receivedTimestamp = receivedTimestamp;
        this.sentTimestamp = sentTimestamp;
        this.to = to;
    }
}
const smsDeliveryInformationEmpty = new SmsDeliveryInformation(DeliveryStatus.PENDING, null, null, null);

class SmsDeliveryStatusResponse {
    constructor(links, deliveryStatus, statusCode) {
        this._links = links;
        this.deliveryStatus = deliveryStatus;
        this.statusCode = statusCode;
    }
}
const smsDeliveryStatusResponseEmpty = new SmsDeliveryStatusResponse([linkEmpty, linkEmpty], [smsDeliveryInformationEmpty, smsDeliveryInformationEmpty], null);

class StartNotifyRequest {
    constructor(accessCode, callbackUrl, clientId, systemId) {
        this.accessCode = accessCode;
        this.callbackUrl = callbackUrl;
        this.clientId = clientId;
        this.systemId = systemId;
    }
}
const startNotifyRequestEmpty = new StartNotifyRequest(null, null, null, null);

class StartNotifyResponse {
    constructor(accessCode, callbackUrl, message, status, statusCode) {
        this.accessCode = accessCode;
        this.callbackUrl = callbackUrl;
        this.message = message;
        this.status = status;
        this.statusCode = statusCode;
    }
}
const startNotifyResponseEmpty = new StartNotifyResponse(null,null, null, Status.UNSUCCESSFUL, null);

class StopNotifyResponse {
    constructor(correlator, message, status, statusCode) {
        this.correlator = correlator;
        this.message = message;
        this.status = status;
        this.statusCode = statusCode;
    }
}
const stopNotifyResponseEmpty = new StopNotifyResponse(null,null, Status.UNSUCCESSFUL, null);

app.post('/sms', function (req, res) {
    let sendSMS = sendSmsEmpty;

    let sendSmsRequest = sendSmsRequestEmpty;
    sendSmsRequest.body = "Test send sms on sandbox";
    sendSmsRequest.from = "9999999";
    sendSmsRequest.notificationUrl = "https://sandbox.api.mtn.com/v1/messages/sms-listeners/messages/sandboxcallback";
    sendSmsRequest.to = ["276633355555"];

    if (req.body.from === sendSmsRequest.from){
        sendSMS.traceId = "600001200401190627110856765471";
        sendSMS.messageId = null;
        sendSMS.smsDeliveryStatus = DeliveryStatus.PENDING;
        sendSMS.smsStatusURL = null;
        sendSMS.statusCode = 0;
        sendSMS.to = ["276633355555"];
    }
    else {
        sendSMS.traceId = null;
        sendSMS.messageId = null;
        sendSMS.smsDeliveryStatus = DeliveryStatus.UNSUCCESSFUL;
        sendSMS.smsStatusURL = null;
        sendSMS.statusCode = 4000;
        sendSMS.to = req.body.to;
    }
    return res.send(JSON.stringify(sendSMS));
});

app.post('/sms-listeners', function (req, res) {
    let startNotify = startNotifyResponseEmpty;

    let startNotifyRequest = startNotifyRequestEmpty;
    startNotifyRequest.accessCode = "32240";
    startNotifyRequest.callbackUrl = "https://sandbox.api.mtn.com/v1/messages/sms-listeners/messages/sandboxcallback";
    startNotifyRequest.clientId = "MADAPI";

    if (req.body.accessCode === startNotifyRequest.accessCode){
        startNotify.status = Status.SUCCESSFUL;
        startNotify.message = "Your message listener has been successfully enabled.";
        startNotify.callbackUrl = "https://sandbox.api.mtn.com/v1/messages/sms-listeners/messages/sandboxcallback";
        startNotify.accessCode = "32240";
        startNotify.statusCode = 0;
    }
    else {
        startNotify.status = Status.UNSUCCESSFUL;
        startNotify.message = "Invalid input.";
        startNotify.callbackUrl = "https://sandbox.api.mtn.com/v1/messages/sms-listeners/messages/sandboxcallback";
        startNotify.accessCode = req.body.accessCode;
        startNotify.statusCode = 5000;
    }
    return res.send(JSON.stringify(startNotify));
});

app.delete('/sms-listeners/1', function (req, res) {
    let stopNotifyResponse = stopNotifyResponseEmpty;

    stopNotifyResponse.status = Status.SUCCESSFUL;
    stopNotifyResponse.correlator = "55555";
    stopNotifyResponse.message = "Your message listener has been successfully disabled.";
    stopNotifyResponse.statusCode = 0;

    return res.send(JSON.stringify(stopNotifyResponse));
});

app.get('/sms/0/status', function (req, res) {
    let smsDeliveryStatusResponse = new SmsDeliveryStatusResponse();
    let smsDeliveryInformation = new SmsDeliveryInformation();

    smsDeliveryInformation.to = "276633355555";
    smsDeliveryInformation.sentTimestamp = "";
    smsDeliveryInformation.receivedTimestamp = "";
    smsDeliveryInformation.deliveryStatus = DeliveryStatus.DELIVERED_TO_NETWORK;

    smsDeliveryStatusResponse.deliveryStatus = [smsDeliveryInformation];
    smsDeliveryStatusResponse.statusCode = 0;

    return res.send(JSON.stringify(smsDeliveryStatusResponse));
});

app.get('/sms/1/status', function(req, res) {
    let smsDeliveryStatusResponse = new SmsDeliveryStatusResponse();

    smsDeliveryStatusResponse.deliveryStatus = [];
    smsDeliveryStatusResponse.statusCode = 3001;

    return res.send(JSON.stringify(smsDeliveryStatusResponse));
});

var server = app.listen(process.env.PORT || 3000, function () {
    console.log('Listening on port %d', server.address().port);
});
